/////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
// @brief   Lab 07c - My First Cat - EE 205 - Spr 2022
//
// @file    hello3.cpp
// @version 1.0 - Initial implementation
//
// Prints "Meow" using a class instead of regular methods
//
// @author Lyon Singleton lyonws@hawaii.edu
// @@date   03/02/2022
//
// 
//////////////////////////////////////////////////////////////






#include <iostream>

using namespace std;
class Cat {
public:
 void sayHello() {
 cout << "Meow\x0A" ;
 }
} ;



int main() {
Cat myCat;
myCat.sayHello();
return 0;

}

