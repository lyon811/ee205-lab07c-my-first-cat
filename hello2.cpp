/////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
// @brief   Lab 07c - My First Cat - EE 205 - Spr 2022
//
// @file    hello2.cpp
// @version 1.0 - Initial implementation
//
// Prints "Hello World!" without using namespaces
//
// @author Lyon Singleton lyonws@hawaii.edu
// @@date   03/02/2022
//
//
//////////////////////////////////////////////////////////////




#include <iostream>

int main() {

std::cout << "Hello World!\x0A" ;
return 0;
}